#!/usr/bin/env bash

#################################
# include the -=magic=-
# you can pass command line args
#
# example:
# to disable simulated typing
# . ../demo-magic.sh -d
#
# pass -h to see all options
#################################
. ./demo/magic.sh


########################
# Configure the options
########################

#
# speed at which to simulate typing. bigger num = faster
#
# TYPE_SPEED=20

#
# custom prompt
#
# see http://www.tldp.org/HOWTO/Bash-Prompt-HOWTO/bash-prompt-escape-sequences.html for escape sequences
#
DEMO_PROMPT="${GREEN}➜ ${CYAN}\W ${COLOR_RESET}"

# text color
DEMO_COMMENT_COLOR=$WHITE

# hide the evidence
clear

# enters interactive mode and allows newly typed command to be executed
cmd

########################################################################################################################################################
########################################################################################################################################################
########################################################################################################################################################

BASE_URL="http://catalysm-rc.local"

function httpget() {
  curl -s -X GET $BASE_URL$1 | jq
}

function httpput() {
  curl -s -X PUT $BASE_URL$1
}

pei "# Deploy the application using kubectl"

pe "kubectl apply -f data/kubectl-deploy.yml"

# Janky way to make sure the pods are up and running before executing next command
sleep 3

pei "kubectl get pods"

# Plain kubectl works but there is no way to easily deploy multiple environments

pei "httpget /dev/about"

pe "kubectl delete -f data/kubectl-deploy.yml"

pei "# Now, 'for real': using Helm!"

# Helm has the 'secret sauce': templating! By passing different configuration files, we can deploy multiple environments

pe "helm install --namespace demo-dev --create-namespace --wait -f values/dev.yml load-tester-dev ./helm"

pei "kubectl get pods -n demo-dev"

pei "httpget /dev/about"

pei "# Let's deploy the production environment too"

pe "helm install --namespace demo-prod --create-namespace --wait -f values/prod.yml load-tester-prod ./helm"

pei "kubectl get pods -n demo-prod"

# The prod env sets up multiple replicas of the app and automatically load balances between them
# The response from these requests will include the OS hostname, which will show how the request get routed differently.

pei "httpget /prod/about"
pei "httpget /prod/about"
pei "httpget /prod/about"

# Now lets deploy the autoscaling production environment

pei "# Let's deploy the autoscaling production environment"

pe "helm upgrade --namespace demo-prod -f values/prod-autoscaling.yml load-tester-prod ./helm --wait"

# Lets check out the newly deployed pod. There should be only one replica
pei "kubectl get pods -n demo-prod"

# We let the load tester image start using up CPU
# This will trigger the HPA to scale up the number of replicas
pe "httpput /prod/cpu"

# Speaker should explain some stuff here so the HPA has time to recognize the load and scale up

# Let's see them scaled up!

pei "kubectl get pods -n demo-prod"


p "# Cleanup"

pei "helm uninstall --namespace demo-prod load-tester-prod"
pei "helm uninstall --namespace demo-dev load-tester-dev"

# sleep 5
# clear

pei "# Thank you for watching!"
pei "# You can find the source code for this demo at https://gitlab.com/rightcrowd-examples/techshare-k8s-environments"
