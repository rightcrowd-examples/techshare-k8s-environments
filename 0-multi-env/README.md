# Tech share

This repository is a collection of files related to a demo about managing multiple environments (production, development, ...) in Kubernetes.

The demo will deploy a [simple application](https://github.com/Ticto/node-load-tester) in a Kubernetes cluster. We'll deploy a development and production environment next to each other with different configurations. We will also upgrade the production environment to a auto-scaling deployment. 

## Setup

These steps are to be done BEFORE giving the demo. They are not part of the demo itself.

```sh
# Note that there is a hardcoded IP address inside the kind-config. Adjust it to yours.
kind create cluster --config=kind-config.yml

kubectl cluster-info # Should report data about your newly-created kind cluster

# Install ingress nginx controller
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml

# Kind does not bundle metrics server. We can install it with the following steps 
# [Source](https://gist.github.com/sanketsudake/a089e691286bf2189bfedf295222bd43)
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.5.0/components.yaml
kubectl patch deployment metrics-server -n kube-system --patch "$(cat data/metrics-patch.yml)"

# You can use this to load Lens with the kind cluster
kind get kubeconfig > data/kubeconfig.yml

# The demo magic script requires `pv` to simulate typing
apt-get install pv

# !!! Make sure you are in the right Kubernetes cluster !!!
k config use-context kind-kind
```

## Demo

```sh
./demo/demo.sh
```





Introduce load-tester-image
httpget/httpput -> MEANS TALKING TO APP


watch curl -s http://catalysm-rc.local/prod/about   


TABS:

 - https://github.com/Ticto/node-load-tester